package co.com.sofka.stepdefinitions.digimed.ingresopaciente;

import co.com.sofka.stepdefinitions.setup.SetUp;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static co.com.sofka.questions.digimed.SuccessfulAttentionAlert.theSuccessfulAttentionAlert;
import static co.com.sofka.questions.mailsac.PatientNotificationFunction.patientNotificationFunction;
import static co.com.sofka.questions.mailsac.PatientScheduledAppointment.patientScheduledAppointment;
import static co.com.sofka.tasks.digimed.AssignTreatment.assignTheTreatment;
import static co.com.sofka.tasks.digimed.CheckInPatient.checkInPatient;
import static co.com.sofka.tasks.digimed.EnterExistingPatient.enterExistingPatient;
import static co.com.sofka.tasks.digimed.FillDiagnosis.fillDiagnosis;
import static co.com.sofka.tasks.digimed.FillSymptomsForm.fillSymptomsForm;
import static co.com.sofka.tasks.digimed.LogIn.logInWith;
import static co.com.sofka.tasks.digimed.OpenLoginPage.openLoginPage;
import static co.com.sofka.tasks.mailsac.OpenInbox.openInbox;
import static co.com.sofka.utils.digimed.enums.ValidationPopup.VALIDATION_POPUP;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class IngresoPacienteStepsDefinitions extends SetUp {
    @Before
    public void setUp() {
        generalSetUp();
    }

    @Dado("que el medico se encuentra logueado en el aplicativo digimed")
    public void queElMedicoSeEncuentraLogueadoEnElAplicativoDigimed(DataTable credentials) {
        theActorInTheSpotlight().wasAbleTo(openLoginPage(), logInWith(credentials));
    }

    @Cuando("se dirige al apartado nuevo paciente e ingresa el documento del nuevo paciente con sus datos")
    public void seDirigeAlApartadoNuevoPacienteEIngresaElDocumentoDelNuevoPacienteConSusDatos() {
        theActorInTheSpotlight().attemptsTo(checkInPatient());
    }

    @Cuando("diligencia el formulario de sintomas del paciente")
    public void diligenciaElFormularioDeSintomasDelPaciente() {
        theActorInTheSpotlight().attemptsTo(fillSymptomsForm());
    }

    @Cuando("se le asigna un diagnostico")
    public void seLeAsignaUnDiagnostico() {
        theActorInTheSpotlight().attemptsTo(fillDiagnosis());
    }

    @Cuando("ingresa el tratamiento y selecciona la {string} para remitir al paciente")
    public void ingresaElTratamientoYSeleccionaLaParaRemitirAlPaciente(String option) {
        theActorInTheSpotlight().attemptsTo(assignTheTreatment().withThe(option));
    }

    @Cuando("verifica la bandeja de entrada del email {string} de la {string} asignada")
    public void verificaLaBandejaDeEntradaDelEmailDeLaAsignada(String email, String nurse) {
        theActorInTheSpotlight().attemptsTo(openInbox().with(email).ofThe(nurse));
    }

    @Entonces("debera mostrarle un mensaje de {string}")
    public void deberaMostrarleUnMensajeDe(String expectedMessage) {
        theActorInTheSpotlight().should(seeThat(patientNotificationFunction(), containsString(expectedMessage)));
    }

    //@RemitirAlPaciente
    @Entonces("el sistema debera mostrarle un mensaje de atencion finalizada")
    public void elSistemaDeberaMostrarleUnMensajeDeAtencionFinalizada() {
        theActorInTheSpotlight().should(seeThat(theSuccessfulAttentionAlert(), equalTo(VALIDATION_POPUP.isValue())));
    }

    //@DarDeAltaAlPaciente
    @Cuando("se dirige al apartado nuevo paciente e ingresa el documento del paciente ya existente")
    public void seDirigeAlApartadoNuevoPacienteEIngresaElDocumentoDelPacienteYaExistente() {
        theActorInTheSpotlight().attemptsTo(enterExistingPatient());
    }

    @Cuando("verifica la cita asignada en la bandeja de entrada del email del {string} {string}")
    public void verificaLaCitaAsignadaEnLaBandejaDeEntradaDelEmailDel(String patient, String email) {
        theActorInTheSpotlight().attemptsTo(openInbox().with(email).ofThe(patient));
    }

    @Entonces("debera ver el correo con el mensaje {string}")
    public void deberaVerElCorreoConElMensaje(String expectedMessage) {
        theActorInTheSpotlight().should(seeThat(patientScheduledAppointment(), containsString(expectedMessage)));
    }
}
