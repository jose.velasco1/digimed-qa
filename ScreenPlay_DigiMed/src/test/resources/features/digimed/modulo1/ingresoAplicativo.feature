# language: es
# author: Esteban

Característica: Ingreso de medico al aplicativo
  Como medico
  quiero poder ingresar a la plataforma DigiMed
  para tener acceso a las herramientas

  @IngresoExitoso
  Escenario: Ingreso Exitoso
    Dado el medico se encuentra en el aplicativo Digimed
    Cuando se dirige al apartado de ingreso y se intenta logear con las credenciales
      | user               | password |
      | medico@digimed.com | 123456   |
    Entonces deberia de ver la main page del aplicativo

  @IngresoFallido
  Escenario: Ingreso Fallido
    Dado el medico se encuentra en el aplicativo Digimed
    Cuando se dirige al apartado de ingreso y se intenta logear con credenciales incorrectos
      | user                      | password |
      | medicofallido@digimed.com | 123458   |
    Entonces el sistema deberia mostrarle el mensaje de error "El Usuario ingresado no es válido"




