package co.com.sofka.userinterfaces.digimed;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class NewPatientPage extends PageObject {
    public static final Target VERIFY_DNI =
            Target.the("Verify dni")
                    .located(By.id("exampleInputDni1"));

    public static final Target VERIFY_BUTTON =
            Target.the("Verify button")
                    .located(By.xpath("//button[@type='submit']"));

    public static final Target NAME_FIELD =
            Target.the("Name field")
                    .located(By.xpath("//input[@id='exampleInputName']"));

    public static final Target DNI =
            Target.the("Dni")
                    .located(By.id("exampleInputDNI"));

    public static final Target EPS =
            Target.the("EPS")
                    .located(By.id("exampleInputEPS"));

    public static final Target EMAIL =
            Target.the("exampleInputEmail1")
                    .located(By.id("exampleInputEmail1"));

    public static final Target CELLPHONE =
            Target.the("Cellphone")
                    .located(By.id("exampleInputCelular"));

    public static final Target SEND_BUTTON =
            Target.the("Send button")
                    .located(By.xpath("//button[@type='submit'][contains(string(), 'Enviar')]"));
}
