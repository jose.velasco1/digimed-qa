package co.com.sofka.userinterfaces.mailsac;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class InboxPage extends PageObject {
    public static final Target PATIENT_TREATMEN_MAIL =
            Target.the("Patient treatmen mail")
                    .located(By.xpath("//strong[contains(string(), 'digimed.sofka@outlook.com')]"));

    public static final Target PATIENT_SCHEDULED_APPOINTMENT =
            Target.the("Patient scheduled appointment")
                    .located(By.xpath("(//td[@class='col-xs-5 ng-binding'][contains(string(), 'DigiMed - Cita programada')])[1]"));

    public static final Target MESSAGE_NOTIFICATION_NURSE =
            Target.the("Message patient notification function")
                    .located(By.xpath("(//p[@class='ng-binding'])[1]"));

    public static final Target SCHEDULED_APPOINTMENT_NOTIFICATION =
            Target.the("ssdsds").located(By.xpath("/html/body/div/div[3]/div[1]/div/div[2]/div/table/tbody/tr[3]/td[2]/p"));
}
