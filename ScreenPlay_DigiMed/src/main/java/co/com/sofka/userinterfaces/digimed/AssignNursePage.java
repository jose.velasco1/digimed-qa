package co.com.sofka.userinterfaces.digimed;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class AssignNursePage extends PageObject {
    public static final Target POPUP_FINISH_ATENTTION =
            Target.the("Popup finish atention")
                    .located(By.xpath("//div[@class='swal2-container swal2-top-end swal2-backdrop-show']"));

    public static final Target SELECT_FUNCTION_SUPERVISION =
            Target.the("Select function supervision")
                    .located(By.xpath("(//*[@id='flexRadioDefault1'])[2]"));

    public static final Target CONTINUE_TO_EDIT_FUNCTION =
            Target.the("Continue to edit function")
                    .located(By.id("btn-siguiente"));

    public static final Target SAVE_AND_CONTINUE =
            Target.the("Save and continue")
                    .located(By.xpath("//button[@id='btn-siguiente'][contains(string(), 'guardar y continuar')]"));

    public static final Target SELECT_NURSE =
            Target.the("Select nurse")
                    .located(By.xpath("//td[contains(string(),'Maria')]//following::input[@id='flexRadioDefault1']"));

    public static final Target FINISH_BUTTON =
            Target.the("Finish button")
                    .located(By.id("btn-finalizar"));
}
