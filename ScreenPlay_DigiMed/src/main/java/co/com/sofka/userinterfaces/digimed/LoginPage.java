package co.com.sofka.userinterfaces.digimed;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class LoginPage extends PageObject {
    public static final Target USER_EMAIL =
            Target.the("User email")
                    .located(By.id("exampleInputEmail1"));

    public static final Target USER_PASSWORD =
            Target.the("User password")
                    .located(By.id("exampleInputPassword1"));

    public static final Target SUBMIT_BUTTON =
            Target.the("Submit button")
                    .located(By.xpath("//button[@type='submit']"));

    public static final Target INVALID_USER_MESSAGE =
            Target.the("Invalid user message")
                    .located(By.xpath("//div[@class='alert alert-danger fade show']"));
}
