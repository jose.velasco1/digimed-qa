package co.com.sofka.userinterfaces.digimed;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class SymptomsFormPage extends PageObject {
    public static final Target SYMPTOMS_FORM_FIELD =
            Target.the("Symptoms form fill")
                    .located(By.id("exampleFormControlTextarea1"));

    public static final Target CONFIRM_SYMPTOMS_BUTTON =
            Target.the("Confirm symptoms button")
                    .located(By.id("btn-ingresar-sintomas"));

}
