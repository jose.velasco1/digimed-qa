package co.com.sofka.tasks.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.conditions.Check;

import static co.com.sofka.tasks.digimed.DischargedPatient.dischargedPatient;
import static co.com.sofka.tasks.digimed.EnterPatient.enterPatient;
import static co.com.sofka.tasks.digimed.TransferPatient.transferPatient;
import static co.com.sofka.userinterfaces.digimed.TreatmentFormPage.*;
import static co.com.sofka.utils.digimed.enums.OptionValues.*;
import static co.com.sofka.utils.digimed.faker.FakerPatientInformation.fakerTreatment;

public class AssignTreatment implements Task {
    private String option;
    private String treatment;

    public AssignTreatment withThe(String option) {
        this.option = option;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        treatment = fakerTreatment();
        actor.attemptsTo(
                Click.on(TREATMENT_FORM_FIELD),
                SendKeys.of(treatment).into(TREATMENT_FORM_FIELD),
                Check.whether(option.equals(CHECKIN.getValue()))
                        .andIfSo(enterPatient())
                        .otherwise(Check.whether(option.equals(RELEASE.getValue()))
                                .andIfSo(dischargedPatient())
                                .otherwise(Check.whether(option.equals(TRANSFER.getValue()))
                                        .andIfSo(transferPatient())))
        );
    }

    public static AssignTreatment assignTheTreatment() {
        return new AssignTreatment();
    }
}
