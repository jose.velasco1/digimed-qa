package co.com.sofka.tasks.digimed;

import co.com.sofka.userinterfaces.digimed.TreatmentFormPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

public class TransferPatient implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(TreatmentFormPage.RADIO_INPUT_TRANSFER),
                Click.on(TreatmentFormPage.BUTTON_FINISH_ATTENTION)
        );
    }
    public static TransferPatient transferPatient(){
        return new TransferPatient();
    }
}
