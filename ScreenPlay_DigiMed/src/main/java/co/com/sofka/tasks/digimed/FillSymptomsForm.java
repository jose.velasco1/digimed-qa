package co.com.sofka.tasks.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;

import static co.com.sofka.userinterfaces.digimed.SymptomsFormPage.*;
import static co.com.sofka.utils.digimed.faker.FakerPatientInformation.fakerSymptomsForm;


public class FillSymptomsForm implements Task {
    private String form;

    @Override
    public <T extends Actor> void performAs(T actor) {
        form = fakerSymptomsForm();
        actor.attemptsTo(
                Click.on(SYMPTOMS_FORM_FIELD),
                SendKeys.of(form).into(SYMPTOMS_FORM_FIELD),
                Click.on(CONFIRM_SYMPTOMS_BUTTON)
        );
    }

    public static FillSymptomsForm fillSymptomsForm() {
        return new FillSymptomsForm();
    }
}
