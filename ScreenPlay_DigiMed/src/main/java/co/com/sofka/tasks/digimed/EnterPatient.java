package co.com.sofka.tasks.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.sofka.userinterfaces.digimed.AssignNursePage.*;
import static co.com.sofka.userinterfaces.digimed.TreatmentFormPage.*;

public class EnterPatient implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(RADIO_INPUT_CHECKIN),
                Click.on(BUTTON_FINISH_ATTENTION),
                Click.on(POPUP_FINISH_ATENTTION),
                Click.on(SELECT_FUNCTION_SUPERVISION),
                Click.on(CONTINUE_TO_EDIT_FUNCTION),
                Click.on(SAVE_AND_CONTINUE),
                Click.on(SELECT_NURSE),
                Click.on(FINISH_BUTTON)
        );
    }

    public static EnterPatient enterPatient() {
        return new EnterPatient();
    }
}
