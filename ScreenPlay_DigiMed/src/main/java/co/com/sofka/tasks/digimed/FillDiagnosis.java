package co.com.sofka.tasks.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;

import static co.com.sofka.userinterfaces.digimed.DiagnosisPage.*;
import static co.com.sofka.utils.digimed.faker.FakerPatientInformation.fakerDiagnosis;

public class FillDiagnosis implements Task {
    private String diagnosis;

    @Override
    public <T extends Actor> void performAs(T actor) {
        diagnosis = fakerDiagnosis();
        actor.attemptsTo(
                Click.on(DIAGNOSIS_FORM),
                SendKeys.of(diagnosis).into(DIAGNOSIS_FORM),
                Click.on(SEND_DIAGNOSIS)
        );
    }

    public static FillDiagnosis fillDiagnosis() {
        return new FillDiagnosis();
    }
}
