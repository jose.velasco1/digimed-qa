package co.com.sofka.interaction.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.Keys;

public class HitTripleArrowLeft implements Task {
    private Target target;

    public HitTripleArrowLeft(Target target) {
        this.target = target;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Hit.the(Keys.LEFT).into(target),
                Hit.the(Keys.LEFT).into(target),
                Hit.the(Keys.LEFT).into(target),
                Hit.the(Keys.LEFT).into(target)
        );
    }

    public static HitTripleArrowLeft on(Target target) {
        return new HitTripleArrowLeft(target);
    }
}
