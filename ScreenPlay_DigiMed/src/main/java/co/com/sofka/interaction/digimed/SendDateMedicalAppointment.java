package co.com.sofka.interaction.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.Keys;

import static co.com.sofka.utils.digimed.enums.DateMedicalAppointment.DATE_HOUR;
import static co.com.sofka.utils.digimed.enums.DateMedicalAppointment.DATE_MEDICAL_APPOINTMENT;

public class SendDateMedicalAppointment implements Task {
    private Target target;

    public SendDateMedicalAppointment(Target target) {
        this.target = target;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                HitTripleArrowLeft.on(target),
                SendKeys.of(DATE_MEDICAL_APPOINTMENT.getValue()).into(target),
                Hit.the(Keys.ARROW_RIGHT).into(target),
                SendKeys.of(DATE_HOUR.getValue()).into(target),
                Hit.the(Keys.ARROW_UP).into(target)
        );
    }

    public static SendDateMedicalAppointment on(Target target) {
        return new SendDateMedicalAppointment(target);
    }

}
