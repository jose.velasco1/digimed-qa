package co.com.sofka.utils.digimed.enums;

public enum ValidationPopup {
    VALIDATION_POPUP(false);

    public final boolean value;

    ValidationPopup(boolean value) {
        this.value = value;
    }

    public boolean isValue() {
        return value;
    }
}
