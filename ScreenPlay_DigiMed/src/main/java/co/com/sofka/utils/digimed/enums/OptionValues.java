package co.com.sofka.utils.digimed.enums;

public enum OptionValues {

    CHECKIN("Ingresar"),
    RELEASE("Dar de alta"),
    TRANSFER("Remitir a otra institución");

    private final String value;

    OptionValues(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
