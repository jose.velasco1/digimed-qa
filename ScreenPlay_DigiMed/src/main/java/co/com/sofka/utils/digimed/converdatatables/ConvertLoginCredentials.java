package co.com.sofka.utils.digimed.converdatatables;

import co.com.sofka.models.digimed.LoginCredentials;
import io.cucumber.datatable.DataTable;

public class ConvertLoginCredentials {
    private ConvertLoginCredentials() {
    }

    public static LoginCredentials convertDataTableToLoginInfo(DataTable dataTable) {
        LoginCredentials loginCredentials = new LoginCredentials();
        loginCredentials.setUser(dataTable.cell(1, 0));
        loginCredentials.setPassword(dataTable.cell(1, 1));
        return loginCredentials;
    }
}
