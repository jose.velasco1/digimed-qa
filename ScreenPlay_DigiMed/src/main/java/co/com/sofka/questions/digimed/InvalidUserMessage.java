package co.com.sofka.questions.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static co.com.sofka.userinterfaces.digimed.LoginPage.INVALID_USER_MESSAGE;

public class InvalidUserMessage implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(INVALID_USER_MESSAGE).answeredBy(actor);
    }

    public static InvalidUserMessage invalidUserMessage() {
        return new InvalidUserMessage();
    }
}
