package co.com.sofka.questions.digimed;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Visibility;

import static co.com.sofka.userinterfaces.digimed.MainPage.SUCCESSFUL_ATTENTION_ALERT;

public class SuccessfulAttentionAlert implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor) {
        return Visibility.of(SUCCESSFUL_ATTENTION_ALERT).answeredBy(actor);
    }

    public static SuccessfulAttentionAlert theSuccessfulAttentionAlert() {
        return new SuccessfulAttentionAlert();
    }
}
